#include <stdio.h>
#include <string.h>

void concat() {
    char string1[50];
    char string2[50];

    printf("Enter the first string: ");
    fgets(string1, sizeof(string1) * sizeof(char), stdin);
    if (!strchr(string1, '\n')) {
        while (fgetc(stdin) != '\n');
    }

    printf("Enter the second string: ");
    fgets(string2, sizeof(string2) * sizeof(char), stdin);
    if (!strchr(string2, '\n')) {
        while (fgetc(stdin) != '\n');
    }

    string1[strlen(string1) - 1] = ' ';

    strcat(string1, string2);
    printf("The concatnated string: %s", string1);
}

int main() {
    concat();
    return 0;
}